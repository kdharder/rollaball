﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform playerPosition;
    Vector3 difference = Vector3.zero;

	// Use this for initialization
	void Start () {
        difference = transform.position - playerPosition.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = playerPosition.position + difference;
	}
}
